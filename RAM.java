import java.io.*;
import java.util.*;

public class RAM {
    File ramFile;
    ArrayList<String> mem = new ArrayList<String>();

    public void loadRAM(String filename) throws IOException {
    	  ramFile = new File(filename);
          String token = new String();
          String c = new String();
          int num = 0;
         
             Scanner scan = new Scanner(new BufferedReader(new FileReader(ramFile)));
              scan.useDelimiter("(?<=.)");

              while(scan != null) {
                  c = scan.next().trim();
                  if(c == (" ") || c == ("\n")) continue;
                  if(c.equals("B")) { 
                	  c = scan.nextLine().trim();
                   while(scan != null) {
                       c = scan.next().trim();
                       if(c.equals(" ")) continue;
                         if(c.equals("[")) {
                          num = Integer.parseInt(scan.next().trim().concat(scan.next().trim()),16);
                             scan.next().trim(); scan.next().trim();
                               num = (Integer.parseInt(scan.next().trim().concat(scan.next().trim()),16)) - num;
                                     }
                                   if(c.equals(":")) {
                                      token = "";
                                        while(scan != null) {
                                         c = scan.next().trim();
                                            if(c == (" ")) continue;
                                             if(c.equals(";")) {
                                                 mem.add(token);
                                                  while(num > 0) {
                                                     mem.add(token);
                                                      num--;
                                                  }
                                                      break;
                                           } else token = token + c;
                                              }
                                          }
                                          if(c.equals("-") && scan.next().trim().equals("-")) {
                                              scan.useDelimiter("\\n");
                                              c = scan.next().trim();
                                              scan.useDelimiter("(?<=.)");
                                          }
                                          if(c.equals("E")) { 
                                        	  c = scan.next().trim();
                                              if(c.equals("N")) { 
                                            	  c = scan.next().trim();
                                                  if(c.equals("D")) { 
                                                	  c = scan.next().trim();
                                                      break;
                                                  }
                                              }
                                          }
                                      }
                                      break;
                                  } else continue;
              }
              scan.close();
          
      }
    
    public void store(short address, String _data) {
        mem.set(address,_data.substring(0, 8));
        mem.set(address + 1,_data.substring(8, 16));
        System.out.println("Add:" + address + "data" + _data);
    }

    public short getMDR(short address) {
        return (short)Integer.parseInt(mem.get(address).concat(mem.get(address + 1)),2);
    }
}