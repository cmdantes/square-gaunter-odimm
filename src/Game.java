
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import javax.swing.SwingUtilities;
import javax.swing.Timer;

public class Game extends Canvas implements Runnable {
	
	private static final long serialVersionUID = 1442798787354930462L;
	public static final int WIDTH = 648, HEIGHT = WIDTH / 12*9;
	private Thread thread;
	private boolean running = false;
	private Random r;
	private HUD hud;
	private Handler handler;
	private Spawner spawner;
	private Menu menu;
	private Player player;
	private PBuletts bullet;
	public LinkedList<EntityDaiichi> ei;
	public LinkedList<EntityDaini> en;
	public final List<Integer> path = new ArrayList<Integer>();
	public SquareBlock sq;
	public int pathIndex;
	
	public Maze m;
	public DepthFirst df;
	public enum STATE{
		Menu,
		Help,
		Game,
		End
	};
	
	public static STATE gameState = STATE.Menu;
	 public Game() {
		this.requestFocus();
		handler = new Handler(this);
		m = new Maze();
		new Window(WIDTH, HEIGHT,"SQUARE GAUNTER O'DIMM",this);
		hud = new HUD();
		menu = new Menu(this,handler,hud, null);
		this.addKeyListener(new KeyInput(handler));
		this.addMouseListener(new Menu(this, handler, hud, null));
		//m = new Maze();
		ei=handler.getEntityDaiichi();
		en=handler.getEntityDaini();
		DepthFirst.searchPath(m, 1, 1, path);
	    pathIndex = path.size() - 2;
		this.setVisible(true);// this \ gets chage to \\ or / in java
		this.setFocusable(true);
		
		spawner = new Spawner(handler,hud);
		player = new Player(32,32,null, handler);
		player.setVisible(true);
		handler.createLevel();
		r = new Random();
		if(gameState == STATE.Game) {
		}
		
	}

	public synchronized void start() {
	thread = new Thread(this);
	thread.start();
	running = true;
	}
	public synchronized void stop() {
		try {
			thread.join();
			running=false;
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
	public void run() {
		setVisible(true);
		 this.requestFocus();
		 long lastTime = System.nanoTime();
	        double amountOfTicks = 60.0;
	        double ns = 1000000000 / amountOfTicks;
	        double delta = 0;
	        long timer = System.currentTimeMillis();
	        int frames = 0;
	        while(running)
	        {
	                    long now = System.nanoTime();
	                    delta += (now - lastTime) / ns;
	                    lastTime = now;
	                    while(delta >=1)
	                            {
	                                tick();
	                                delta--;
	                            }
	                            if(running)
	                                render();
	                            frames++;
	                            
	                            if(System.currentTimeMillis() - timer > 1000)
	                            {
	                                timer += 1000;
	                                System.out.println("FPS: "+ frames);
	                                frames = 0;
	                            }
	        }		
	        		
	                stop();
		
	}

	private void tick(){
		handler.tick();
		if(gameState == STATE.Game) {
			
			hud.tick();
		//	c.tick();
			spawner.tick();
			
			if (HUD.HEALTH <=0) {
				HUD.HEALTH=100;
				gameState= STATE.End;
				handler.clearEnemys();
				
			}
		}else if (gameState == STATE.Menu || gameState == STATE.End) {
			menu.tick();
			
		}
	}

	private void render(){
		BufferStrategy bs = this.getBufferStrategy();
		if(bs == null) {
			this.createBufferStrategy(3);
			return;
		}
		Graphics g = bs.getDrawGraphics();
		Graphics2D g2d = (Graphics2D) g;
		super.paint(g);
		//m.render(g2d);
		handler.render(g);
		if(gameState == STATE.Game) {
			if(Game.gameState == Game.STATE.Game) {
				
				hud.render(g);
			}
			  
		}else if (gameState == STATE.Menu || gameState == STATE.Help || gameState == STATE.End) {
			g.setColor(Color.black);
			g.fillRect(0, 0, WIDTH, HEIGHT);
			menu.render(g);
			
		}
		g.dispose();
		bs.show();
		
	}
	   
	public static float clamp(float var, float min, float max) {
		//for clamp so that the player can't get out
		if(var>=max)
			return var = max;
		else if(var <= min)
			return var = min;
		else
			return var;
	}
	public static void main(String args[]) {
	new Game();
		
	}

}
