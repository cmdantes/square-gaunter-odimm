import java.awt.Graphics;
import java.awt.Rectangle;

public interface EntityDaini {
	public void tick();
	public void render(Graphics g);
	public Rectangle getBounce();
	
	public float getX();
	public float getY();
	public ID getId();
}
