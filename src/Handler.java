import java.awt.Graphics;
import java.util.ArrayList;
import java.util.LinkedList;

public class Handler {
	LinkedList<GameObject> object = new LinkedList<GameObject>();
	LinkedList<PBuletts> bullet = new LinkedList<PBuletts>();
	LinkedList<EntityDaiichi> ei = new LinkedList<EntityDaiichi>();
	LinkedList<EntityDaini> en = new LinkedList<EntityDaini>();
	private Game game;
	SquareBlock sq;
	EntityDaiichi enta;
	EntityDaini	entb;
	public Handler (Game game) {
		this.game=game;
	}
	
	public void tick() {
		for(int i = 0; i <ei.size(); i++) {
			enta = ei.get(i);
			enta.tick();
		}
		for(int i = 0; i <en.size(); i++) {
		 entb = en.get(i);
			entb.tick();
		}
		for(int i = 0; i <object.size(); i++) {
			GameObject tempObject = object.get(i);
			tempObject.tick();
		}
		for (int i=0; i<bullet.size(); i++) {
			GameObject TempObject = bullet.get(i);
			TempObject.tick();
		}
		
	}
	public void render(Graphics g) {
		for(int i = 0; i < ei.size(); i++) {
			enta = ei.get(i);
			enta.render(g);
		}
		for(int i = 0; i < en.size(); i++) {
			entb = en.get(i);
			 entb.render(g);
		}
		for(int i = 0; i < object.size(); i++) {
			GameObject tempObject = object.get(i);
			tempObject.render(g);
		}
		for (int i=0; i<bullet.size(); i++) {
			GameObject TempObject = bullet.get(i);
			TempObject.render(g);
		}
	}


	public void clearEnemys() {
	
	for(int i = 0; i<object.size(); i++) {
			GameObject tempObject = object.get(i);
			
			if(tempObject.getId() != ID.Player || tempObject.getId() != ID.SquareBlock) removeObject(tempObject);{
				object.clear();
				if(Game.gameState != Game.STATE.End) {
					addObject(new Player((int)tempObject.getX(), (int)tempObject.getY(),ID.Player,this));
				}

			}
		}}
	public void addEntity(EntityDaiichi block) {
		this.ei.add(block);
	}
	public void removeEntity(EntityDaiichi block) {
		this.ei.remove(block);
	}
	public void addEntity(EntityDaini block) {
		this.en.add(block);
	}
	public void removeEntity(EntityDaini block) {
		this.en.remove(block);
	}
	public LinkedList<EntityDaiichi> getEntityDaiichi(){
		return ei;
	}
	public LinkedList<EntityDaini> getEntityDaini(){
		return en;
	}
	/*public void addBullet(PBuletts block) {
		this.bullet.add(block);
	}
	public void removeBullet(PBuletts block) {
		this.bullet.remove(block);
	}*/
	public void addObject(GameObject object) {
		this.object.add(object);
	}
	public void removeObject(GameObject object) {
		this.object.remove(object);
	}
	public void createLevel() {
		
		for(int xx=0; xx<Game.WIDTH+32; xx+=32) {
			addObject(new SquareBlock(xx,Game.HEIGHT-50,ID.SquareBlock));
		}
		for(int xx=50; xx<Game.WIDTH+100; xx+=100) {
			addObject(new SquareBlock(xx,Game.HEIGHT-100,ID.SquareBlock));
			addObject(new SquareBlock(xx,Game.HEIGHT-100,ID.SquareBlock));
		}
		for(int xx=50; xx<Game.WIDTH+200; xx+=200) {
			addObject(new SquareBlock(xx,Game.HEIGHT-200,ID.SquareBlock));
		}
		for(int xx=20; xx<Game.WIDTH+300; xx+=300) {
			addObject(new SquareBlock(xx,Game.HEIGHT-300,ID.SquareBlock));
		}
		for(int xx=0; xx<Game.WIDTH+350; xx+=350) {
			addObject(new SquareBlock(xx,Game.HEIGHT-400,ID.SquareBlock));
		}
		for(int xx=50; xx<Game.WIDTH+532; xx+=532) {
			addObject(new SquareBlock(xx,Game.HEIGHT-532,ID.SquareBlock));
			addObject(new SquareBlock(xx,Game.HEIGHT-532,ID.SquareBlock));
		}


	
	
	}}
