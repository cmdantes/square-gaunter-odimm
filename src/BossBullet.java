import java.awt.Color;
import java.awt.Rectangle;
import java.util.Random;
import java.awt.Graphics;

public class BossBullet extends GameObject implements EntityDaini{
	private Handler handler;
	Random r = new Random();
	public BossBullet(int x, int y, ID id, Handler handler) {
		super(x, y, id);
		this.handler=handler;
		velX = (r.nextInt(5 - -5)+ -5);
		velY = 5;
	}
	public Rectangle getBounce(){
        return new Rectangle((int) x,(int) y, 16, 16);
}

	public void tick() {
		x += velX;
		y += velY;
		
		//if(y<=0 || y>=Game.HEIGHT - 32) velY*= -1;
		//if(x<=0 || x>=Game.HEIGHT - 16) velX*= -1;
		 if(y>= Game.HEIGHT)handler.removeEntity(this);
		//handler.addEntity(new Trail((int)x,(int) y, ID.Trail, Color.blue, 16, 16, 0.02f,handler));//dito babaguhinnung trail sa float
		 //handler.addEntity((EntityDaiichi) new Trail((int)x,(int) y, ID.Trail, Color.blue, 16, 16, 0.02f,handler));//dito babaguhinnung trail sa float
			
	}

	public void render(Graphics g) {
		
		g.setColor(Color.red);
		g.fillRect((int)x,(int)y,16,16);
	}

}
