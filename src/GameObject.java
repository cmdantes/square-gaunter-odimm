import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.util.ArrayList;

import javax.swing.JPanel;

public abstract class GameObject {
//dito gagawa ng characters
	protected float x,y;
	public JPanel panel;
    protected ID id;
    protected float velX, velY;
	private Image ic;
	protected int tileX;
	  int tileY;
    public GameObject(float x, float y, ID id){
            this.x = x;
            this.y = y;
            this.id = id;
    }
    public abstract void tick();
    public abstract void render(Graphics g);
    public abstract Rectangle getBounce();
    public void setX(int x){
            this.x = x;
    }
    public void setY(int y){
            this.y = y;
    }
    public float getX(){
            return x;
    }
    public float getY(){
            return y;
    }
    public void setID(ID id){
            this.id = id;
    }
    public ID getId(){
            return id;
    }
    public void setVelX(int velX){
            this.velX = velX;
    }
    public void setVelY(int velY){
            this.velY = velY;
    }
    public float getVelX(){
            return velX;
    }
    public float getVelY(){
            return velY;
    }public void setTileX(int tileX){
        this.tileX = tileX;
    }
    public void setTileY(int tileY){
        this.tileY = tileY;
    }
    public float getTileX(){
        return tileX;
    }
    public float getTileY(){
        return tileY;
    }
	

   
}