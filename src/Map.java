import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.awt.*;
import java.io.*;
import java.util.*;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Map {
	
	private Scanner m;
	
	private String Map[]= new String[14];
	
	private Image grass,wall;
	
	public Map() {
		ImageIcon img = new ImageIcon("res/dsa.png");
		grass=img.getImage();
		img = new ImageIcon("res/dsa.png");
		wall=img.getImage();
		
		openFile();
		readFile();
		closeFile();
	}
	public Image getGrass() {
		return grass;
	}
	public Image getWall() {
		return wall;
	}
	public String getMap(int x, int y) {
		String index = Map[y].substring(x,x+1);
		return index;
	}
	public void openFile() {
		try {
			m = new Scanner(new File("res/Level.txt"));
		}catch(Exception e) {
			System.out.println("error loading map");
		}
	}
	public void readFile() {
		while(m.hasNext()) {
			for(int i=0; i<14; i++) {
				Map[i] = m.next();
			}
		}
	}
	public void closeFile() {
		m.close();
	}}