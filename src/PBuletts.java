import java.awt.Color;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.ImageIcon;
import javax.swing.Timer;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;

public class PBuletts extends GameObject implements EntityDaiichi{
	private Handler handler;
	 private GameObject player;
	private Game game;
	KeyInput keyInput;
	 public PBuletts(float x, float y,KeyInput keyInput, Handler handler,Game game) {
			super(x,y,null);
			this.x=x;
			this.y=y;
			this.handler=handler;
			this.game=game;
		}


	public Rectangle getBounce(){
	     return new Rectangle((int) x,(int) y, 32, 32);
	}
	public void tick() {
		y-=10;
		for(int i = 0; i < handler.en.size(); i++){
            GameObject en = (GameObject) handler.en.get(i);
            if(en.getId() == ID.BasicEnemy || en.getId()==ID.FastEnemy||en.getId()== ID.SmartEnemu||en.getId()== ID.Boss){
                            if(getBounce().intersects(en.getBounce())){
                            	HUD.HEALTH += 1;
                            }
                    }
            }
	
	}


	public void render(Graphics g) {
	
		Graphics2D g2d = (Graphics2D) g;
		g2d.drawImage(getBulletImg(),(int) x,(int) y, null);}
	
	public Image getBulletImg() {
		ImageIcon ic  = new ImageIcon("res/bullet.png");
		return ic.getImage();	}


	@Override
	public KeyInput keyInput() {
		// TODO Auto-generated method stub
		return null;
	}
    
	

}
