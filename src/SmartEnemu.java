import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
public class SmartEnemu extends GameObject implements EntityDaini{
	 private Handler handler;
     private GameObject player;
     private Game game;
     public SmartEnemu(int x, int y, ID id, Handler handler,Game game){
             super(x,y,id);
             this.handler = handler;
             this.game=game;
             //ALGO	
             for(int i = 0; i < handler.ei.size(); i++ ){
                     if(handler.ei.get(i).getId() == ID.Player) 
                    	 player = (GameObject) handler.ei.get(i);
             }    
     }
     public Rectangle getBounce(){
         return new Rectangle((int) x,(int) y, 16, 16);
 }
     public void tick(){
         x += velX;
         y += velY;
         float diffX = x - player.getX() - 16;
         float diffY = y - player.getY() - 16;
         float distance = (float) Math.sqrt((x-player.getX())*(x-player.getX())+(y-player.getY())*(y-player.getY()));
         velX = ((-1 / distance) * diffX);
         velY = ((-1 / distance) * diffY);
         collision();
        }


private void collision(){
for(int i = 0; i < handler.object.size(); i++){
    GameObject tempObject = (GameObject) handler.object.get(i);
    if(tempObject.getId()==ID.SquareBlock){
                    if(getBounce().intersects(tempObject.getBounce())){
                   	 y=tempObject.getY()-32;
                   	 velY=0;
                       
                    
                    }}
            }
    }
     public void render(Graphics g){
         g.setColor(Color.green);
         g.fillRect((int)x,(int)y,16,16);
 }
}
