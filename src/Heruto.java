import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Random;

import javax.swing.ImageIcon;


public class Heruto extends GameObject{
	//	private Controller c;
        Random r = new Random();
        Handler handler;
        private ImageIcon icon; 
        private Image image;
        public Heruto(int x,int y, ID id, Handler handler){
                super(x, y, id);
                this.handler = handler;
                velX = 5;
        		velY = 5;
       
        }
        public Rectangle getBounce(){
                return new Rectangle((int) x,(int) y, 32, 32);
        }
    

        public void tick(){
        	x += velX;
    		y += velY;
    		
        	if(y<=0 || y>=Game.HEIGHT - 32) velY*= -1;
    		if(x<=0 || x>=Game.HEIGHT - 16) velX*= -1;
         //       handler.addObject(new Trail( x, y, ID.Trail, Color.white, 32, 32, 0.05f,handler));
              
        }
  
 
        public void render(Graphics g) {
    		Graphics2D g2d = (Graphics2D) g; //casting new variable graphics
    	//	g.setColor(Color.green); 
    		//g2d.draw(getBounce());	//para dun sa collision
    		g2d.drawImage(getPlayerImg(),(int) x,(int) y, null);
    		
    	}
    	public Image getPlayerImg() {
    		ImageIcon ic  = new ImageIcon("res/dsa.png");
    	
    		return ic.getImage();
    	}

}/*
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.ImageIcon;

public class Player extends GameObject {

	// Random r = new Random();
     Handler handler;
     private int speed = 5;
    // Controller c;
     private Image img;
     private ImageIcon icon; 
     private int x,y;
     public Player(int x,int y, ID id, Handler handler){
             super(x, y, id);
             this.x = x;
             this.y = y;
             x=40;
             y=60;
             this.handler = handler;
             //this.c = c;
     }
   
     public Rectangle getBounce(){
             return new Rectangle((int) x,(int) y, 16, 16);
     }
     
     public void tick(){
         x += velX;
         y += velY;     
  
         x = (int) Game.clamp(x, 0, Game.WIDTH -37);                          
         y = (int) Game.clamp(y, 0, Game.HEIGHT - 60);
        // handler.addObject(new Trail((int) x,(int) y, ID.Trail, Color.cyan, 32, 32, 0.05f,handler));
        handler.addObject(new PBuletts((int)x, (int)y,id, handler));
		
         collision();
 }
	private void collision() {// dito na mababawasan health mo
		for(int i = 0; i < handler.object.size(); i++){
            GameObject tempObject = handler.object.get(i);
            if(tempObject.getId() == ID.BasicEnemy || tempObject.getId()==ID.FastEnemy||tempObject.getId()== ID.SmartEnemu || tempObject.getId()==ID.Boss){
                            if(getBounce().intersects(tempObject.getBounce())){
                                    HUD.HEALTH -= 2;
                            }
                    }
            }
    }
	
	public void render(Graphics g) {
		Graphics2D g2d = (Graphics2D) g; //casting new variable graphics
	//	g.setColor(Color.green); 
		//g2d.draw(getBounce());	//para dun sa collision
		g2d.drawImage(getPlayerImg(),(int) x,(int) y, null);
		
	}
	public Image getPlayerImg() {
		ImageIcon ic  = new ImageIcon("res/dsa.png");
		return ic.getImage();
	}


}
*/
/*import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.ImageIcon;

public class Player extends GameObject {
	  private Image image;
	  private int speed = 5;
	  private boolean visible = false;
	  private ArrayList<PBuletts> missiles = new ArrayList<PBuletts>();
	  private Handler handler;
	  private int width;
	    private int height;
	    private int x = 40;
        private int y = 60;
	 
     public Player(int x,int y, ID id, Handler handler){
             super(x, y, id);
             this.handler = handler;
             ImageIcon ii = new ImageIcon("craft.png");
         	image = ii.getImage();
         	visible = true;
         	height = image.getHeight(null);
     		width = image.getWidth(null);
     		x=40;
     		y=60;
     		 
     }
     
     public Image getImage() {
         return image;
     }
     
     public void fire() {
     	missiles.add(new PBuletts(x + width,y + height/2 - 3));
     }
     
     public ArrayList<PBuletts> getMissiles() {
     	return missiles;
     }
     public Rectangle getBounce(){
         return new Rectangle((int) x,(int) y, 32, 32);
 }
     public boolean isVisible() {
         return visible;
     }
     public void keyPressed(KeyEvent e) {
    	 int key = e.getKeyCode();
    	 if (key == KeyEvent.VK_SPACE) {
             fire();
         }
     }
  
     public void tick(){
         x += velX;
         y += velY;     
  
         x = (int) Game.clamp(x, 0, Game.WIDTH -37);                          
         y = (int) Game.clamp(y, 0, Game.HEIGHT - 60);
         handler.addObject(new Trail((int) x,(int) y, ID.Trail, Color.cyan, 32, 32, 0.05f,handler));
         collision();
        
         
 }
	private void collision() {// dito na mababawasan health mo
		for(int i = 0; i < handler.object.size(); i++){
            GameObject tempObject = handler.object.get(i);
            if(tempObject.getId() == ID.BasicEnemy || tempObject.getId()==ID.FastEnemy||tempObject.getId()== ID.SmartEnemu || tempObject.getId()==ID.Boss){
                            if(getBounce().intersects(tempObject.getBounce())){
                                    HUD.HEALTH -= 2;
                            }
                    }
            }
    }
	public void render(Graphics g) {
		//Graphics2D g2d = (Graphics2D) g; //casting new variable graphics
		//g.setColor(Color.green); 
		//g2d.draw(getBounce());	//para dun sa collision
		g.setColor(Color.white);
        g.fillRect((int)x,(int)y,32,32);       
		
	}

}
*/

/*
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.Random;
public class Player extends GameObject{
    private  Handler handler;
     private BufferedImage player;
     Game game;

     	public Player(int x, int y,Game game, Handler handler){
            super(x,y,null);
     		this.x=x;
            this.y=y;
            this.handler=handler;
            SpriteSheet ss = new SpriteSheet(game.getSpriteSheet());
            player = ss.grabImage(1, 1, 32, 32);
     }


	public Rectangle getBounce(){
             return new Rectangle((int) x,(int) y, 32, 32);
     }
  
     public void tick(){
    	 x += velX;
         y += velY;     
  
         x = Game.clamp(x, 0, Game.WIDTH -37);                          
         y = Game.clamp(y, 0, Game.HEIGHT - 60);
       //  handler.addObject(new Trail((int) x,(int) y, ID.Trail, Color.cyan, 32, 32, 0.05f,handler));
        
         collision();
       
         
 }
	private void collision() {// dito na mababawasan health mo
		for(int i = 0; i < handler.object.size(); i++){
            GameObject tempObject = handler.object.get(i);
            if(tempObject.getId() == ID.BasicEnemy || tempObject.getId()==ID.FastEnemy||tempObject.getId()== ID.SmartEnemu || tempObject.getId()==ID.Boss){
                            if(getBounce().intersects(tempObject.getBounce())){
                                    HUD.HEALTH -= 2;
                            }
                    }
            }
    }
	public void render(Graphics g) {
		//Graphics2D g2d = (Graphics2D) g; //casting new variable graphics
		//g.setColor(Color.green); 
		//g2d.draw(getBounce());	//para dun sa collision
		g.drawImage(player,(int) x,(int) y, null);
		g.setColor(Color.white);
       // g.fillRect((int)x,(int)y,32,32);       
		
	}
	
	

}*/
