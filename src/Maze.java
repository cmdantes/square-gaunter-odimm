import java.awt.Graphics;
import java.awt.Color;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.awt.*;
import java.io.*;
import java.util.*;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

	public class Maze extends JFrame{
		 public static int rows = 20;
		    public static int columns = 20;
		    public static int panelSize = 25;
		    public static int map[][] = new int[columns][rows];
		    public static int endLevelLoc;
	private String Maze[] = new String [14];
	
	KeyInput keyInput;
	public Player player;
	Handler handler;
	public	Game game;
	Graphics g;
	 public static int [][] maze = 
	        { {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
	          {1,0,1,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
	          {1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
	          {1,0,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
	          {1,0,1,0,0,0,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,1},
	          {1,0,1,0,1,1,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,9},
	          {1,0,1,0,1,0,0,0,1,1,1,0,1,1,1,1,1,1,1,1,1,1},
	          {1,0,1,0,1,1,1,0,1,0,1,0,1,1,1,1,1,1,1,1,1,1},
	          {1,0,0,0,0,0,0,0,0,0,1,9,1,1,1,1,1,1,1,1,1,1},
	          {1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
	          {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
	          {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
	          {1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
	          {1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,1},
	          {1,0,0,0,0,0,0,0,0,0,1,0,1,1,1,1,1,1,1,1,1,1},
	          {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}
	        };
	  private final List<Integer> path = new ArrayList<Integer>();
	  private int pathIndex;
	    
	
public void Maze() {
	player = new Player(pathIndex, pathIndex, null, handler);
	player.setVisible(true);
	handler.addEntity(new Player(Game.WIDTH/2-32 ,Game.HEIGHT/2-32 ,ID.Player, handler));
	for(int y = 0; y < columns; y++){
        for(int x = 0; x < rows; x++){
        	  Tile tile = new Tile(x, y);
              tile.setSize(panelSize, panelSize);
                 tile.setLocation((x*panelSize)+23, (y*panelSize)+25);
                 if(maze[x][y] == 0){
                     tile.setBackground(Color.GRAY);
                 }else{
                     tile.setBackground(Color.GREEN);
                     tile.setWall(false);
                     if(x == 0){
                     	player.setLocation((x*panelSize)+23, (y*panelSize)+25);
                     	player.y = y;
                     }
                     if(x == columns-1){
                     	endLevelLoc = y;
                     }
                 }
        }
	/*
	  for (int y = 0; row < maze.length; y++) {
          for (int x = 0; col < maze[1].length; x++) {
              Color color;
              switch (maze[row][col]) {
                  case 1 : color = Color.BLACK; break;
                  case 9 : color = Color.RED; break;
                  default : color = Color.WHITE;
              }
              g.setColor(color);
              g.fillRect(30 * col, 30 * row, 30, 30);
              g.setColor(Color.BLACK);
              g.drawRect(30 * col, 30 * row, 30, 30);
          }
	  }*/
}}}



