import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.LinkedList;
import java.util.Random;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;


public class Player extends GameObject implements EntityDaiichi{
	Player player;
	//	private Controller c;
	JPanel panel = new JPanel();
	JFrame frame = new JFrame();
        Random r = new Random();
        Handler handler;
        private ImageIcon icon; 
        private Image image;
        private boolean visible = false;
        private Game game;
        int tx, ty;
      //  Maze m;
        
        public Player(int x,int y, ID id, Handler handler){
                super(x, y, id);
                this.handler = handler;
        }
        
	  public void tick(){
		  
		 
               x += velX;
               y += velY;     
                x = Game.clamp(x,0, Game.WIDTH -37);                          
                y = Game.clamp(y, 0, Game.HEIGHT - 60);
                handler.addObject(new Trail( x, y, ID.Trail, Color.yellow, 5, 5, 0.05f,handler));
  
                collision();
        }
        
        
  
        private void collision(){
        	 for(int i = 0; i < handler.en.size(); i++){
                 GameObject en = (GameObject) handler.en.get(i);
                 if(en.getId() == ID.BasicEnemy || en.getId()==ID.FastEnemy||en.getId()== ID.SmartEnemu||en.getId()== ID.Boss){
                                 if(getBounce().intersects(en.getBounce())){
                                      HUD.HEALTH -= 2;
                                 }
                         }
                 }
        	 for(int i = 0; i < handler.object.size(); i++){
                 GameObject tempObject = (GameObject) handler.object.get(i);
                 if(tempObject.getId()==ID.SquareBlock){
                                 if(getBounce().intersects(tempObject.getBounce())){
                                	 y=tempObject.getY()-32;
                                	velY=0;
                                    
                                 
                                 }
                                 if(getBounceTop().intersects(tempObject.getBounce())){
                                	 y=tempObject.getY()+16;
                                	 velY=0;
                                    
                                	 System.out.println("das");
                                 
                                 }
                                 if(getBounceRight().intersects(tempObject.getBounce())){
                                	 y=tempObject.getY()+32/2/32;
                                	 velY=0;
                                    
                                	 System.out.println("das");
                                 
                                 }
                                 if(getBounceRight().intersects(tempObject.getBounce())){
                                	 x=tempObject.getX()+32;
                                	 velY=0;
                                 }
                                 if(getBounceRight().intersects(tempObject.getBounce())){
                                	 x=tempObject.getX()+32;
                                	 
                                 }
                         }
                 }
        	
        	 
        }
        public void render(Graphics g) {
    		Graphics2D g2d = (Graphics2D) g; //casting new variable graphics
    	//	g.setColor(Color.green); 
    		//g2d.draw(getBounce());	//para dun sa collision
    		g2d.drawImage(getPlayerImg(),(int) x,(int) y, null);
    		g.setColor(Color.RED);
    		g2d.draw(getBounce());
    		g2d.draw(getBounceRight());
    		g2d.draw(getBounceLeft());
    		g2d.draw(getBounceTop());
    	}
        public Rectangle getBounce(){
            return new Rectangle((int) x,(int) y, 32 ,32);
    }
        public Rectangle getBounceTop(){
            return new Rectangle((int) x,(int) y, 32 ,32);
    }
        public Rectangle getBounceLeft(){
            return new Rectangle((int) x,(int) y, 32 ,32);
    }
        public Rectangle getBounceRight(){
            return new Rectangle((int) x,(int) y, 32 ,32);
    }
    	public Image getPlayerImg() {
    		ImageIcon ic  = new ImageIcon("res/dsa.png");
    		visible = true;
    		return ic.getImage();
    	}
		@Override
		public KeyInput keyInput() {
			game.repaint();
			return null;
		}
		public void setVisible(boolean b) {
			// TODO Auto-generated method stub
			
		}
		public void setLocation(float f, float g) {
			// TODO Auto-generated method stub
			
		}
}