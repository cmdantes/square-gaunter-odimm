import java.util.Random;

public class Spawner {
	
	private Handler handler;
	private HUD hud;
	private Random r = new Random();
	private int scoreKeep = 0;
	
	public Spawner(Handler handler, HUD hud) {
		this.handler = handler;
		this.hud = hud;
	}
	public void tick() {
		scoreKeep++;
		if(scoreKeep >= 100) {
			scoreKeep = 0;
			hud.setLevel(hud.getLevel()+1);
			if(hud.getLevel()==1) {
				handler.addEntity(new BasicEnemy(r.nextInt(Game.WIDTH-100), r.nextInt(Game.HEIGHT-100), ID.BasicEnemy, handler));
				handler.addEntity(new Boss(r.nextInt(Game.WIDTH-100), r.nextInt(Game.HEIGHT-100), ID.Boss, handler));
				
			}
			if(hud.getLevel()==2) {
				handler.addEntity(new BasicEnemy(r.nextInt(Game.WIDTH-100), r.nextInt(Game.HEIGHT-100), ID.BasicEnemy, handler));
				}
		if(hud.getLevel()==4) {
			handler.addEntity(new FastEnemy(r.nextInt(Game.WIDTH/2)-48 ,r.nextInt(Game.HEIGHT-120) ,ID.FastEnemy, handler));	
				}
			if(hud.getLevel()==6) {
				handler.addEntity(new FastEnemy(r.nextInt(Game.WIDTH/2)-48 ,r.nextInt(Game.HEIGHT-120) ,ID.FastEnemy, handler));	}
			if(hud.getLevel()==8) {
				handler.addEntity(new BasicEnemy(r.nextInt(Game.WIDTH-100), r.nextInt(Game.HEIGHT-100), ID.BasicEnemy, handler));
				handler.addEntity(new BasicEnemy(r.nextInt(Game.WIDTH-100), r.nextInt(Game.HEIGHT-100), ID.BasicEnemy, handler));
			}
			if(hud.getLevel()==10) {
				handler.addEntity(new SmartEnemu(r.nextInt(Game.WIDTH - 50), r.nextInt(Game.HEIGHT - 50), ID.SmartEnemu, handler, null));
				handler.addEntity(new FastEnemy(r.nextInt(Game.WIDTH/2)-48 ,r.nextInt(Game.HEIGHT-120) ,ID.FastEnemy, handler));	
				handler.addEntity(new BasicEnemy(r.nextInt(Game.WIDTH-100), r.nextInt(Game.HEIGHT-100), ID.BasicEnemy, handler));
				handler.addEntity(new BasicEnemy(r.nextInt(Game.WIDTH-100), r.nextInt(Game.HEIGHT-100), ID.BasicEnemy, handler));
			}
			if(hud.getLevel()==12) {
				handler.addEntity(new SmartEnemu(r.nextInt(Game.WIDTH - 50), r.nextInt(Game.HEIGHT - 50), ID.SmartEnemu, handler, null));
				handler.addEntity(new SmartEnemu(r.nextInt(Game.WIDTH - 50), r.nextInt(Game.HEIGHT - 50), ID.SmartEnemu, handler, null));
				handler.addEntity(new SmartEnemu(r.nextInt(Game.WIDTH - 50), r.nextInt(Game.HEIGHT - 50), ID.SmartEnemu, handler, null));
				handler.addEntity(new FastEnemy(r.nextInt(Game.WIDTH/2)-48 ,r.nextInt(Game.HEIGHT-120) ,ID.FastEnemy, handler));	
				handler.addEntity(new BasicEnemy(r.nextInt(Game.WIDTH-100), r.nextInt(Game.HEIGHT-100), ID.BasicEnemy, handler));
				handler.addEntity(new BasicEnemy(r.nextInt(Game.WIDTH-100), r.nextInt(Game.HEIGHT-100), ID.BasicEnemy, handler));
				}	
			if(hud.getLevel()==14) {
				handler.addEntity(new SmartEnemu(r.nextInt(Game.WIDTH - 50), r.nextInt(Game.HEIGHT - 50), ID.SmartEnemu, handler, null));
				handler.addEntity(new SmartEnemu(r.nextInt(Game.WIDTH - 50), r.nextInt(Game.HEIGHT - 50), ID.SmartEnemu, handler, null));
				handler.addEntity(new SmartEnemu(r.nextInt(Game.WIDTH - 50), r.nextInt(Game.HEIGHT - 50), ID.SmartEnemu, handler, null));
				handler.addEntity(new FastEnemy(r.nextInt(Game.WIDTH/2)-48 ,r.nextInt(Game.HEIGHT-120) ,ID.FastEnemy, handler));	
				handler.addEntity(new BasicEnemy(r.nextInt(Game.WIDTH-100), r.nextInt(Game.HEIGHT-100), ID.BasicEnemy, handler));
				handler.addEntity(new BasicEnemy(r.nextInt(Game.WIDTH-100), r.nextInt(Game.HEIGHT-100), ID.BasicEnemy, handler));		
				handler.addEntity(new BasicEnemy(r.nextInt(Game.WIDTH-100), r.nextInt(Game.HEIGHT-100), ID.BasicEnemy, handler));
		
		}
	}
	

}
}