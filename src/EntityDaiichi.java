import java.awt.Graphics;
import java.awt.Rectangle;

public interface EntityDaiichi {
	public void tick();
	public void render(Graphics g);
	public Rectangle getBounce();
	public KeyInput keyInput();
	public float getX();
	public float getY();
	public ID getId();
	
}
