import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Menu extends MouseAdapter{

	private Game game;
	private HUD hud;
	private Handler handler;
	private Graphics g;
	public Menu(Game game, Handler handler, HUD hud,Graphics g) {
		this.game=game;
		this.hud =hud;
		this.handler=handler;
		this.g=g;
	}
	public void mousePressed(MouseEvent e) {
		int mx = e.getX();
		int my = e.getY();
		
		//play button
		if(Game.gameState == Game.STATE.Menu) {
		if (mouseOver(mx,my,210, 150, 200, 64)) {
			Game.gameState = Game.STATE.Game;
			
			handler.addEntity(new Player(Game.WIDTH/2-32 ,Game.HEIGHT/2-32 ,ID.Player, handler));
			handler.addEntity(new BasicEnemy((Game.WIDTH/2)-48 ,-120 ,ID.BasicEnemy, handler));
			handler.addEntity(new SmartEnemu((Game.WIDTH/2)-48 ,-120 ,ID.SmartEnemu, handler, game));
			handler.addEntity(new FastEnemy((Game.WIDTH/2)-48 ,-120 ,ID.FastEnemy, handler));
			
			//handler.clearEnemys();
			handler.addEntity (new Boss((Game.WIDTH/2)-48 ,-120 ,ID.Boss, handler));
		
		        
			}
		//help button
		if(mouseOver(mx,my,210, 250, 200, 64)){
			Game.gameState = Game.STATE.Help;
		}
		
		//quit button
		if(mouseOver(mx,my,210,350,100,64)) {
			System.exit(1);
		}
		
	}
		if(Game.gameState == Game.STATE.Help) { 
		if(mouseOver(mx,my,210, 350, 200, 64)) {
			Game.gameState = Game.STATE.Menu;
			return;

					}
				}
		if(Game.gameState == Game.STATE.End) { 
			if(mouseOver(mx,my,210, 350, 200, 64)) {
				System.exit(1);
						}
					}
			}
		
	
	public void mouseReleased(MouseEvent e) {
		
	}
	
	private boolean mouseOver(int mx, int my, int x, int y, int width, int height) {
		if (mx > x && mx < x + width) {
			if(my > y && my < y + height) {
				return true;
			}else return false;
		}else return false;
	}
	public void tick() {
		
	}
	
	public void render(Graphics g) {
		
	if(Game.gameState == Game.STATE.Menu){
		
			Font fnt = new Font("serif",Font.BOLD,50);
			Font fnt2 = new Font("arial",1,50);
			g.setFont(fnt);
			g.setColor(Color.lightGray);
			g.drawString("Square",225,50);
			g.drawString("Gaunter O'Dimm",125,100);
			
			g.setFont(fnt2);
			g.drawRect(210, 150, 200, 64);
			g.drawString("Play", 240,190);
			

			g.drawRect(210, 250, 200, 64);
			g.drawString("Help", 240,290);
			

			g.drawRect(210, 350, 200, 64);
			g.drawString("Quit", 240,390);
				
		}
	else if (Game.gameState == Game.STATE.Help) {
			Font fnt = new Font("arial",1,50);
			Font fnt2 = new Font("arial",1,40);
			g.setFont(fnt);
			g.setColor(Color.cyan);
			g.drawString("Help", 240,70);
			g.setFont(fnt2);
			g.drawString("Use WASD Keys to survive", 75,240);
			g.setColor(Color.green);
			g.drawString("SPACE to shoot to gain Health", 60,300);
			g.setFont(fnt2);
			g.setColor(Color.lightGray);
			g.drawRect(210, 350, 200, 64);
			g.drawString("Back", 250,390);
		}
		else if (Game.gameState == Game.STATE.End) {
			Font fnt = new Font("arial",1,50);
			Font fnt2 = new Font("arial",1,40);
			Font fnt3 = new Font("calibri",1,40);
			g.setFont(fnt);
			g.setColor(Color.white);
			g.drawString("Game Over", 175,70);
			
			g.setFont(fnt3);
			g.setColor(Color.RED);
			g.drawString("You Lost Your Score is : "+hud.getScore(), 100,200);
			
			g.setFont(fnt2);
			g.setColor(Color.white);
			g.drawRect(210, 350, 200, 64);
			g.drawString("Quit", 250,390);
		}
		
	}

}
