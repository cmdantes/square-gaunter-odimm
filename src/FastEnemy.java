import java.awt.Color;
import java.awt.Rectangle;
import java.awt.Graphics;
import java.awt.Graphics2D;

public class FastEnemy extends GameObject implements EntityDaini{
	private Handler handler;
	public FastEnemy(int x, int y, ID id, Handler handler) {
		super(x, y, id);
		this.handler=handler;
		velX = 2;
		velY = 9;
		
	}
	public Rectangle getBounce() {
		return new Rectangle((int) x,(int) y, 16, 16);
	}

	public void tick() {
		x += velX;
		y += velY;
		
		if(y<=0 || y>=Game.HEIGHT - 32) velY*= -1;
		if(x<=0 || x>=Game.HEIGHT - 16) velX*= -1;
		  // handler.addObject(new Trail((int)x,(int) y, ID.Trail, Color.gray, 16, 16, 0.02f,handler));//dito babaguhinnung trail sa float
		
	}
	
	public void render(Graphics g) {
		
		g.setColor(Color.cyan);
		g.fillRect((int)x, (int)y, 16, 16);
	}

}
